# Scan Algorithms

This sketch is intended to explore various potential display scan algorithms.
The original test hardware that runs it is an Adafruit Grand Central M4,
connected to a SparkFun 4-character 14-segment display.

# Hardware Connections

Goal: connect a display and a button to the Grant Central M4.  [The
display](https://www.sparkfun.com/products/21215) is one that SparkFun
sells (any color version will do).  You can also get it at DigiKey.
The button is any SPST normally-open pushbutton that you happen to
have handy.

This is fairly straightforward.  There are five general classes of connection
between the hardware and the Grand Central:

* Current-limited Anode Connections
* Unlimited Anode Connections
* Current-limited Cathode Connections
* Unlimited Cathode Connections
* The Button.

The "unlimited" connections are the easiest; they hook directly from
the Grand Central on the appropriate GPIO pin to the appropriate
display pin:

Unlimited Cathode Connections:

| Grand Central | Display Pin | 
| :---: | :---: |
|  7 | 14 |
|  8 | 13 |
|  9 |  4 |
| 10 |  3 |
| 11 |  6 |
| 12 | 12 |
| 13 |  5 |

Unlimited Anode Connections:

| Grand Central | Display Pin |
| :---: | :---: |
| 30 | 16 |
| 31 |  1 |
| 32 | 15 |
| 33 |  2 |
| 34 | 11 |
| 35 | 10 |
| 36 |  8 |
| 37 |  9 |

The current-limited ones are a bit different in that they have a
resistor in the mix to limit the current between the display and the
MCU.  They should look like this:

```
 GPIO Pin <----/\/\/\----> Display Pin
               R = 220
```

The connections are as follows:

Limited Cathode Connections:

| Grand Central | Display Pin | 
| :---: | :---: |
|  0 | 14 |
|  1 | 13 |
|  2 |  4 |
|  3 |  3 |
|  4 |  6 |
|  5 | 12 |
|  6 |  5 |

Limited Anode Connections:

| Grand Central | Display Pin |
| :---: | :---: |
| 40 | 16 |
| 41 |  1 |
| 42 | 15 |
| 43 |  2 |
| 44 | 11 |
| 45 | 10 |
| 46 |  8 |
| 47 |  9 |

Connecting to the same pins as the "unlimited" connections is
deliberate; the MCU will activate the appropriate pins at the
appropriate times.

The button is the most complex connection, for various values of
"complex".  It looks like this:

```
          GPIO50     +3.3v
             ^         ^
             |    |    |
     R = 1K  |  --+--  |
  +--/\/\/\--+--o   o--+
  |          |
  |         ---
  |         --- C = 500pF
  |          |
  +----------+
             |
           -----
            ---
             -
```

It's probably wrong (I think maybe the cap should be on the other side
of the resistor), but it seems to work, so I'm not messing with it.
The scope shows what, to me, looks like a happier waveform with this
version.

If anyone wants to play, the alternative is:

``` 
 +3.3v
   ^     |
   |   --+--
   +---o   o---/\/\/\---+---> GPIO 50
                        |
                       ---
                       --- C = 100pF
                        |
                        |
                      -----
                       ---
                        -
```