// Algorithm to display a static letter F on one digit.
// This is intended to allow for current measurements.

#include <Arduino.h>
#include "display.h"

void alg_static_run() {
  digitalWrite(30, HIGH);
  digitalWrite(0, LOW);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
}

void alg_static_stop() {
  digitalWrite(30, LOW);
  digitalWrite(0, HIGH);
  digitalWrite(4, HIGH);
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
}
