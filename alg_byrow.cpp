// Okay, this one is a bit more complex.  We're going to handle
// things on a per-row basis; for each segment, determine which
// digits are using it and energize their anodes.  Note that we
// have to do a bit of gymnastics due to the dual-anode-per-char
// issue.  These displays are very odd.

#include <Arduino.h>
#include "display.h"

static const int rowCount = 7; // We'll process both char halves at once
static const int usPerRow = 1000000/(rowCount*scan_hz);

static const char *dbuf = "FIO5";
//static const char *dbuf = "  O ";

void alg_byrow_run() {
  // convert display buf to glyphs
  const uint16_t gbuf[] = {
    font[dbuf[0]],
    font[dbuf[1]],
    font[dbuf[2]],
    font[dbuf[3]],
  };


  for (int row = 0; row < 7; row++) {
    for (int c = 0; c < 4; c++) {
      for (int i = 0; i < 14; i++) {
	if (display_segments[i].c_pin == row)
	  digitalWrite(digits[c].lo + display_segments[i].a_pin + 10,
		       (gbuf[c] & display_segments[i].mask ? HIGH : LOW));
      }
    }

    // Delay while activated
    digitalWrite(display_segments[row].c_pin + 7, LOW);
    delayMicroseconds(usPerRow);
    digitalWrite(display_segments[row].c_pin + 7, HIGH);
  }
          
}
