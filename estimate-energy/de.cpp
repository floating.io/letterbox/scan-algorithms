#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "../font.c"

const int UA_PER_LIT_SEGMENT = 1800;
const int DIGIT_COUNT        = 12;
const int MODULE_COUNT       = 3;
const int DIGITS_PER_MODULE  = DIGIT_COUNT / MODULE_COUNT;

int segments_in_message(const char *message, int count) {
  int segs = 0;

  for (int i = 0; i < count && i < strlen(message); i++) {
    segs += __builtin_popcount(font[message[i]]);
  }

  return segs;
}


int est_byseg_whole(const char *message) {
  int segs = segments_in_message(message, DIGIT_COUNT);
  return (segs * UA_PER_LIT_SEGMENT) / (DIGIT_COUNT * 14);
}

int est_byseg_module(const char *message) {
  int total = 0;

  for (int i = 0; i < MODULE_COUNT; i++) {
    if (strlen(message) <= i*DIGITS_PER_MODULE)
      break;
    int segs = segments_in_message(message + (DIGITS_PER_MODULE * i), DIGITS_PER_MODULE);
    total += (segs * UA_PER_LIT_SEGMENT) / (DIGITS_PER_MODULE * 14);
  }

  return total;
}

int est_bydigit_whole(const char *message) {
  int total = 0;

  for (int i = 0; i < DIGIT_COUNT && i < strlen(message); i++) {
    total += segments_in_message(message+i, 1) * UA_PER_LIT_SEGMENT;
  }

  // Remember that there are two scans per digit.
  return total / (DIGIT_COUNT*2);
}

int est_bydigit_module(const char *message) {
  int total = 0;

  for (int m = 0; m < MODULE_COUNT; m++) {
    if (strlen(message) <= m*DIGITS_PER_MODULE)
      break;
    
    const char *submsg   = message + (m*DIGITS_PER_MODULE);
    int         subtotal = 0;
    for (int i = 0; i < DIGITS_PER_MODULE && i < strlen(submsg); i++) {
      subtotal += segments_in_message(submsg+i, 1) * UA_PER_LIT_SEGMENT;
    }
    total += subtotal/(DIGITS_PER_MODULE*2);
  }
  
  return total;
}

int est_byrow(const char *message) {
  int total = 0;

  // This function is a great example of what happens when you
  // don't simplify things.  The first version of this fucntion
  // went through and calculated every single row, then averaged
  // the result out.  It was in the neighborhood of 25 lines of
  // code.
  //
  // When you look at the actual result, however, the math is the
  // exact equivalent of doing this:
  return (segments_in_message(message, DIGIT_COUNT) * UA_PER_LIT_SEGMENT) / 7;
  // Go figure.
}


typedef int (*estfunc_t)(const char *message);

typedef struct {
  estfunc_t   estimate;
  const char *name;
} estentry_t;


int main(int argc, char **argv) {
  if (argc != 2) {
    printf("Usage: %s <message>\n", argv[0]);
    exit(1);
  }
  const char *message = argv[1];

  const estentry_t estimators[] = {
    { est_byseg_whole   , "byseg_whole"    },
    { est_byseg_module  , "byseg_module"   },
    { est_bydigit_whole , "bydigit_whole"  },
    { est_bydigit_module, "bydigit_module" },
    { est_byrow         , "byrow"          },
    { NULL, NULL },
  };

  for (const estentry_t *est = estimators; est->estimate; est++) {
    printf("Algorithm [%15s]: %0.3fmA\n", est->name, est->estimate(message) / 1000.0);
  }
}
