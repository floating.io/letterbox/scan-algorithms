#include "display.h"

// The jumbled nature of the pins here is because
// I stole the font from boredom14.  The segment map
// is considerably different on the displays used in
// that project...
//
// Also note that these are expected to appear in this
// array as groups of 7 contiguous entries, and each
// entry within a group should always have an identcal
// anode pin offset.  This is relied upon by some of
// the algorithms.  I'd make it more obvious, but this
// is just an experiment anyway...
const segmap_t display_segments[] = {
  // Lower half
  { 0x0001, 0, 0 }, // A
  { 0x0002, 1, 0 }, // B
  { 0x0004, 2, 0 }, // C
  { 0x0008, 3, 0 }, // D
  { 0x0010, 4, 0 }, // E
  { 0x0020, 5, 0 }, // F
  { 0x1000, 6, 0 }, // N (G1 new)

  // Upper half
  { 0x0100, 0, 1 }, // J (G2 new)
  { 0x2000, 1, 1 }, // P (H new)
  { 0x0040, 2, 1 }, // G (J new)
  { 0x0080, 3, 1 }, // H (K new)
  { 0x0200, 4, 1 }, // K (L new)
  { 0x0400, 5, 1 }, // L (M new)
  { 0x0800, 6, 1 }, // M (N new)
};



// A list of digits and their corresponding anode pins...
const anmap_t digits[] = {
  { 30, 31 },
  { 32, 33 },
  { 34, 35 },
  { 36, 37 },
};
