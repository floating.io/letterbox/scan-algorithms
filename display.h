#include <stdint.h>

#pragma once

extern uint16_t font[];

typedef struct {
  uint16_t mask;
  int      c_pin;
  int      a_pin; // offset from base pin!
} segmap_t;

// Segment map in a pair of bytes:
//
//            GG
//     NMLKJH 21FEDCBA
// 
// Note that the decimal point and the colon
// are currently unsupported.  I haven't yet figured
// out how I want to deal with those, and for this
// test they're irrelevant.
//
extern const segmap_t display_segments[];

typedef struct {
  int lo; // GPIO pin for G2-N
  int hi; // GPIO pin for A-G1
} anmap_t;

extern const anmap_t digits[];

const int scan_hz = 120;
