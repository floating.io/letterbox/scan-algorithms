#include "display.h"
#include "algorithms.h"

const int buttonPin = 20;

// Configure the pins to use the set where the current
// limiting resistors are on the cathode.
void resist_cathode() {
  for (int i = 0; i < 14; i++) {
    pinMode(display_segments[i].c_pin + 7, INPUT);

    pinMode(display_segments[i].c_pin, OUTPUT);
    digitalWrite(display_segments[i].c_pin, HIGH);
  }

  for (int i = 0; i < 4; i++) {
    pinMode(digits[i].lo + 10, INPUT);
    pinMode(digits[i].hi + 10, INPUT);

    pinMode(digits[i].lo, OUTPUT);
    pinMode(digits[i].hi, OUTPUT);
    digitalWrite(digits[i].lo, LOW);
    digitalWrite(digits[i].hi, LOW);
  }
}


// Configure the pins to use the set where the current-limiting
// resistors are on the anode.
void resist_anode() {
  for (int i = 0; i < 14; i++) {
    pinMode(display_segments[i].c_pin, INPUT);

    pinMode(display_segments[i].c_pin + 7, OUTPUT);
    digitalWrite(display_segments[i].c_pin + 7, HIGH);
  }

  for (int i = 0; i < 4; i++) {
    pinMode(digits[i].lo, INPUT);
    pinMode(digits[i].hi, INPUT);

    pinMode(digits[i].lo + 10, OUTPUT);
    pinMode(digits[i].hi + 10, OUTPUT);
    digitalWrite(digits[i].lo + 10, LOW);
    digitalWrite(digits[i].hi + 10, LOW);
  }
}

// General reset.
void reset_display() {
  resist_cathode();

  for (int i = 0; i < 4; i++) {
    digitalWrite(digits[i].lo, LOW);
    digitalWrite(digits[i].hi, LOW);
  }

  for (int i = 0; i < 7; i++) {
    digitalWrite(display_segments[i].c_pin, HIGH);
  }
}


// And here we go...
void setup() {
  pinMode(buttonPin, INPUT);
  reset_display();
}

typedef struct {
  void (*prep)();
  void (*run)();
  void (*stop)();
} algentry_t;


// Main loop.  Duh.
void loop() {
  static int algMode = 0;
  static bool buttonState = digitalRead(buttonPin);

  // The button will rotate through this list of algorithms.  The
  // prep method will select which set of pins to use; the run
  // method will execute one instance of the scan loop.  The stop
  // method will clean up after we switch away from the algorithm,
  // but before the next algorithm is prepped.
  algentry_t algList[] = {
    { resist_cathode, alg_none_run              , reset_display },
    { resist_cathode, alg_static_run            , reset_display },
    { resist_cathode, alg_byseg_whole_run       , reset_display },
    { resist_cathode, alg_byseg_per_module_run  , reset_display },
    { resist_cathode, alg_bydigit_whole_run     , reset_display },
    { resist_cathode, alg_bydigit_per_module_run, reset_display },
    { resist_anode  , alg_byrow_run             , reset_display },
    { NULL, NULL }
  };

  // Run an instance of our currently-selected algorithm.
  algList[algMode].run();

  // Check the button.  Note that this is debounced in
  // hardware; we'll deal with a state transition as
  // appropriate.
  bool newState = digitalRead(buttonPin);
  if (newState != buttonState) {
    buttonState = newState;
    if (buttonState) {
      // Button was just pressed.  Stop the current algorithm and bump
      // to the next one.
      algList[algMode].stop();
      ++algMode;

      // If we went over the end, return to the beginning.
      if (!algList[algMode].run)
        algMode = 0;

      // Prep the hardware for the  new algorithm.
      algList[algMode].prep();
    }
  }
}
