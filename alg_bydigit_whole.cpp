// Scan the display per digit, lighting all relevant segments at once
// for a given digit.

#include <Arduino.h>
#include "display.h"

static const int digitCount   = 12;
static const int usPerDigit   = 1000000/(digitCount*scan_hz);
static const int usPerSegment = usPerDigit / 2;

static const char *dbuf = "FIO3";

static void scan_segments(int anode, const segmap_t *map, char ch) {
  digitalWrite(anode, HIGH);

  uint16_t glyph = font[ch];

  // Bring the segments up, delay, then bring them down.
  for (int i = 0; i < 7; i++)
    digitalWrite(map[i].c_pin, glyph & map[i].mask ? LOW : HIGH);
  delayMicroseconds(usPerSegment);
  digitalWrite(anode, LOW);
}

void alg_bydigit_whole_run() {
  for (int digit = 0; digit < 12; digit++) {
    if (digit > 3) {
      // These digits are not installed; just delay to
      // simulate their existence.
      delayMicroseconds(usPerDigit);
      continue;
    }

    // Scan stuff.  We have to do this twice per digit
    // in order to handle things correctly.

    // Scan lo
    scan_segments(digits[digit].lo, display_segments, dbuf[digit]);

    // Scan hi
    scan_segments(digits[digit].hi, display_segments + 7, dbuf[digit]);

  }
}
