// Similar to alg_bydigit_whole, but assumes that modules are
// running in parallel.

#include <Arduino.h>
#include "display.h"

static const int digitCount   = 4;
static const int usPerDigit   = 1000000/(digitCount*scan_hz);
static const int usPerSegment = usPerDigit / 2;

static const char *dbuf = "FIO4";

static void scan_segments(int anode, const segmap_t *map, char ch) {
  digitalWrite(anode, HIGH);

  uint16_t glyph = font[ch];

  // Bring the segments up, delay, then bring them down.
  for (int i = 0; i < 7; i++)
    digitalWrite(map[i].c_pin, glyph & map[i].mask ? LOW : HIGH);
  delayMicroseconds(usPerSegment);
  digitalWrite(anode, LOW);
}

void alg_bydigit_per_module_run() {
  // Scan stuff.  We have to do this twice per digit
  // in order to handle things correctly.
  for (int mod_digit = 0; mod_digit < 4; mod_digit++) {
    // Scan lo
    scan_segments(digits[mod_digit].lo, display_segments, dbuf[mod_digit]);
    
    // Scan hi
    scan_segments(digits[mod_digit].hi, display_segments + 7, dbuf[mod_digit]);
  }
}
