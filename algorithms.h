#pragma once

extern void alg_none_run();

extern void alg_static_run();
extern void alg_static_stop();

extern void alg_byseg_whole_run();
extern void alg_byseg_per_module_run();

extern void alg_bydigit_whole_run();
extern void alg_bydigit_per_module_run();

extern void alg_byrow_run();
