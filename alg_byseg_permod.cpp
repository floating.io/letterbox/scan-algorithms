// Similar to byseg_whole, but assumes that every 4-character
// module is scanned in parallel.

#include <Arduino.h>
#include "display.h"

static const int digitCount   = 4;
static const int usPerDigit   = 1000000/(digitCount*scan_hz);
static const int usPerSegment = usPerDigit / 14;

static const char *dbuf = "FIO2";

static void scan_segments(int anode, const segmap_t *map, char ch) {
  digitalWrite(anode, HIGH);

  uint16_t glyph = font[ch];

  // Walk through segments doing as we must.
  for (int i = 0; i < 7; i++) {
    digitalWrite(map[i].c_pin, glyph & map[i].mask ? LOW : HIGH);
    delayMicroseconds(usPerSegment);
    //delay(usPerSegment/1000);
    digitalWrite(map[i].c_pin, HIGH);
  }

  digitalWrite(anode, LOW);
}

void alg_byseg_per_module_run() {
  // Scan stuff.  We have to do this twice per digit
  // in order to handle things correctly.
  for (int mod_digit = 0; mod_digit < 4; mod_digit++) {
    // Scan lo
    scan_segments(digits[mod_digit].lo, display_segments, dbuf[mod_digit]);
    
    // Scan hi
    scan_segments(digits[mod_digit].hi, display_segments + 7, dbuf[mod_digit]);
  }
}
